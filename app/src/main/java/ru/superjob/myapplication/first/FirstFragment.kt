package ru.superjob.myapplication.first

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_first.*
import ru.superjob.myapplication.BaseFragment


class FirstFragment : BaseFragment(), FirstPresenterView {
    private val firstPresenter = FirstPresenter()

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        injectPresenter(firstPresenter)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(ru.superjob.myapplication.R.layout.fragment_first, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        firstPresenter.onAttach(this)
    }

    override fun setTextObjectApp(str: String) {
        tv1.text = str
    }

    override fun setTextObjectBaseActivity(str: String) {
        tv2.text = str
    }

    override fun setTextObjectMainActivity(str: String) {
        tv3.text = str
    }

    override fun setTextObjectFragment(str: String) {
        tv4.text = str
    }

    override fun setTextObjectPresenter(str: String) {
        tv5.text = str
    }
}
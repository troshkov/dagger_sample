package ru.superjob.myapplication.first.di

import dagger.Module
import dagger.Provides
import ru.superjob.myapplication.objects.ObjectFragment

@Module
class FirstFragmentModule {

    @Provides
    fun provideObjectFragment() = ObjectFragment()

}
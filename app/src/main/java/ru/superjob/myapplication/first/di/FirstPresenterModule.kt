package ru.superjob.myapplication.first.di

import dagger.Module
import dagger.Provides
import ru.superjob.myapplication.objects.ObjectPresenter

@Module
class FirstPresenterModule {

    @Provides
    fun provideObjectPresenter() = ObjectPresenter()

}
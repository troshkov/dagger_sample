package ru.superjob.myapplication.first

interface FirstPresenterView {

    fun setTextObjectApp(str: String)

    fun setTextObjectBaseActivity(str: String)

    fun setTextObjectMainActivity(str: String)

    fun setTextObjectFragment(str: String)

    fun setTextObjectPresenter(str: String)

}
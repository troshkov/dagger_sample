package ru.superjob.myapplication.first

import android.util.Log
import ru.superjob.myapplication.BasePresenter
import ru.superjob.myapplication.objects.*
import javax.inject.Inject
import javax.inject.Named

class FirstPresenter : BasePresenter<FirstPresenterView>() {

    @Inject
    lateinit var objectApp: ObjectApp

    @Inject
    lateinit var objectBaseActivity: ObjectBaseActivity

    @Inject
    lateinit var objectMainActivity: ObjectMainActivity

    @Inject
    lateinit var objectFragment: ObjectFragment

    @Inject
    lateinit var objectPresenter: ObjectPresenter

    @field:[Inject Named("userName")]
    lateinit var userName: String

    fun onAttach(firstPresenterView: FirstPresenterView) {
        firstPresenterView.setTextObjectApp(objectApp.str)
        firstPresenterView.setTextObjectBaseActivity(objectBaseActivity.str)
        firstPresenterView.setTextObjectMainActivity(objectMainActivity.str)
        firstPresenterView.setTextObjectFragment(objectFragment.str)
        firstPresenterView.setTextObjectPresenter(objectPresenter.str)
        Log.d("ololo", userName)
    }

}
package ru.superjob.myapplication

import android.content.Context
import android.support.v4.app.Fragment
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

open class BaseFragment : Fragment() {
    @Inject
    lateinit var presenterInjector: DispatchingAndroidInjector<BasePresenter<*>>

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    fun injectPresenter(presenter: BasePresenter<*>) {
        presenterInjector.maybeInject(presenter)
    }

}
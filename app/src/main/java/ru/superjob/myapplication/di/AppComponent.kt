package ru.superjob.myapplication.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import ru.superjob.myapplication.App
import javax.inject.Named


@Component(modules = [AndroidSupportInjectionModule::class, AppModule::class, ActivityProvider::class])
interface AppComponent {
    fun inject(app: App)

    @Component.Builder
   interface Builder {
        @BindsInstance
        fun userName(@Named("userName") userName: String): Builder

        fun build(): AppComponent
    }
}
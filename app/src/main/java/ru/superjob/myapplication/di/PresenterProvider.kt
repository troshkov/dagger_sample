package ru.superjob.myapplication.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.superjob.myapplication.first.FirstPresenter
import ru.superjob.myapplication.first.di.FirstPresenterModule

@Module
abstract class PresenterProvider {

    @PresenterScope
    @ContributesAndroidInjector(modules = [FirstPresenterModule::class])
    internal abstract fun provideFirstPresenterFactory(): FirstPresenter

}
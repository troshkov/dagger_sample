package ru.superjob.myapplication.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.superjob.myapplication.first.FirstFragment
import ru.superjob.myapplication.first.di.FirstFragmentModule


@Module
abstract class FragmentProvider {

    @FragmentScope
    @ContributesAndroidInjector(modules = [FirstFragmentModule::class, PresenterProvider::class])
    internal abstract fun provideFirstFragmentFactory(): FirstFragment

}
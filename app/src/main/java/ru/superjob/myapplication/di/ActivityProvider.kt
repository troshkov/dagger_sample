package ru.superjob.myapplication.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.superjob.myapplication.BaseActivity
import ru.superjob.myapplication.MainActivity

@Module
abstract class ActivityProvider {

    @ActivityScope
    @ContributesAndroidInjector(modules = [BaseActivityModule::class])
    abstract fun bindBaseActivity(): BaseActivity

    //Adding subclasses
    @ActivityScope
    @ContributesAndroidInjector(modules = [BaseActivityModule::class, MainActivityModule::class, FragmentProvider::class])
    abstract fun bindMainActivity(): MainActivity

}
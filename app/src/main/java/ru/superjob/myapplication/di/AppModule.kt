package ru.superjob.myapplication.di

import dagger.Module
import dagger.Provides
import ru.superjob.myapplication.objects.ObjectApp


@Module
class AppModule {

    @Provides
    fun provideObjectApp() = ObjectApp()

    @Provides
    fun provideUserName(userName : String) : String {
        return userName
    }



}
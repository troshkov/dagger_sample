package ru.superjob.myapplication.di

import dagger.Module
import dagger.Provides
import ru.superjob.myapplication.objects.ObjectBaseActivity


/*@Module(subcomponents = [ActivityBuilder_BindBaseActivity.BaseActivitySubcomponent::class])*/
@Module
class BaseActivityModule {

    /* @Binds
     @IntoMap
     @ClassKey(BaseActivity::class)
     internal abstract fun bindBaseActivityInjectorFactory(factory: ActivityBuilder_BindBaseActivity.BaseActivitySubcomponent.Factory): AndroidInjector.Factory<*>
 */
    /*@Module
    companion object {
        @JvmStatic
        @Provides
        fun provideObjectBaseActivity() = ObjectBaseActivity()
    }*/
    @Provides
    fun provideObjectBaseActivity() = ObjectBaseActivity()

}
package ru.superjob.myapplication.di

import dagger.Module
import dagger.Provides
import ru.superjob.myapplication.objects.ObjectMainActivity


@Module//(subcomponents = [ActivityBuilder_BindMainActivity.MainActivitySubcomponent::class])
class MainActivityModule {
    /*@Binds
    @IntoMap
    @ClassKey(MainActivity::class)
    abstract fun bindMainActivityInjectorFactory(factory: MainActivitySubcomponent.Factory): AndroidInjector.Factory<*>*/

    /* @Module
     companion object {
         @JvmStatic
         @Provides
         fun provideObjectMainActivity() = ObjectMainActivity()
     }*/
    @Provides
    fun provideObjectMainActivity() = ObjectMainActivity()
}
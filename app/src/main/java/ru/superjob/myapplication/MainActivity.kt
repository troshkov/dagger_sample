package ru.superjob.myapplication

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import ru.superjob.myapplication.objects.ObjectApp
import ru.superjob.myapplication.objects.ObjectBaseActivity
import ru.superjob.myapplication.objects.ObjectMainActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {
    @Inject
    lateinit var objectApp: ObjectApp

    @Inject
    lateinit var objectBaseActivity: ObjectBaseActivity

    @Inject
    lateinit var objectMainActivity: ObjectMainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        Log.d("ololo", objectApp.str)
        Log.d("ololo", objectBaseActivity.str)
        Log.d("ololo", objectMainActivity.str)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
